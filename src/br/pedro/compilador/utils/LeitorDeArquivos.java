package br.pedro.compilador.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LeitorDeArquivos {
	private static String fileName = "/home/pedro/Projetos/Java/Compilador/src/codigoFonte.txt";
	private ArrayList<String> linhas = new ArrayList<>();
	private FileReader fileReader;
	private BufferedReader bufferedReader;
	
	public BufferedReader abrirArquivo(){
		try{
			fileReader = new FileReader(fileName);
			bufferedReader = new BufferedReader(fileReader);
			return bufferedReader;
		}catch(FileNotFoundException e) {
			System.out.println("Nao foi possivel abrir o arquivo");
			return null;
		}
	}
	
	public ArrayList<String> lerLinhas(BufferedReader bfrdr){
		String line;
		try{
			while((line = bfrdr.readLine()) != null){
				this.linhas.add(line);
			}
			return this.linhas;
		}catch(IOException e){
			System.out.println("Erro ao ler o arquivo");
			return null;
		}
	}
	
	public ArrayList<String> getLinhas(){
		return this.linhas;
	}
	
	public static void setFileName(String s){
		fileName = s;
	}
}
