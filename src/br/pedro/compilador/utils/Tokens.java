package br.pedro.compilador.utils;

public class Tokens {
	public static final String PR = "Palavra Reservada";
	public static final String ID = "Identificador";
	public static final String NI = "Número Inteiro";
	public static final String NR = "Número Real";
	public static final String DLM= "Delimitador";
	public static final String ATB= "Comando de Atribuição";
	public static final String REL= "Operador Relacional";
	public static final String ADD= "Operador Aditivo";
	public static final String MUL= "Operador Multiplicativo";
	
}
