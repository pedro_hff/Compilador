package br.pedro.compilador.analisadorlexico;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

import br.pedro.compilador.parser.TabelaDeSimbolos;
import br.pedro.compilador.utils.LeitorDeArquivos;
import br.pedro.compilador.utils.Tokens;

public class AnalisadorLexico {
	private LeitorDeArquivos leitor = new LeitorDeArquivos(); //Classe que lê um arquivo
	private ArrayList<String> arquivo;						  //ArrayList que receberá o arquivo, linha por linha
	private String linha;									  //Receberá a string da linha que está sendo lida
	private int linhaPos;									  //Receberá a posição/index da linha. Importante para controlar o while
	private String lexema;									  //Receberá a palavra que está sendo montada
	private int charPos;									  //Receberá o index do caractere que está sendo lido, relativo à "linha"
	private int c;											  //Controlador do getTokens
	
	//Lê o proximo caractere, caso não encontrado, pula uma linha
	public String readNext(){
		if(!isLastPos()){
			this.charPos++;
			this.lexema += "" + linha.charAt(charPos);
			return this.lexema; 
		}else{
			//Parte importante, caso o tamanho do arraylist de linhas lido do arquivo seja
			//menor que o "linhaPos", adiciona +1 no linhaPos para sair do while na função getTokens()
			if(this.arquivo.size() > (this.linhaPos+1)){
				this.linhaPos++;
				this.linha = this.arquivo.get(linhaPos);
				this.charPos = 0;
				this.lexema = "" + this.linha.charAt(charPos);
				return this.lexema;				
			}
			this.linhaPos++;
			return this.lexema;
		}
	}
	
	//Valida se o caractere lido é o último da linha
	public boolean isLastPos(){
		return (this.linha.length()-1) == this.charPos;
	}
	
	//Um facilitador pra não ficar escrevendo this.lexema.equals toda hora
	public boolean validaLexema(String s){
		return this.lexema.equals(s);
	}
	
	//Retorna o caractere em uma posição da linha atual sem precisar mover o apontador "charPos"
	//Utilizado apenas na função abaixo
	public String getChar(int pos){
		return "" + linha.charAt(pos);
	}
	
	//Utiliza do getChar para verificar se o próximo caractere é um delimitador
	//Uso a função antes de adicionar algo na tabela de simbolos
	public boolean nextIsDelimitador(){
		int next = this.charPos + 1;
		if(this.linha.length() > next)
			return  (this.getChar(next).equals(";")) || 
					(this.getChar(next).equals(".")) || 
					(this.getChar(next).equals("(")) || 
					(this.getChar(next).equals(")")) || 
					(this.getChar(next).equals(",")) || 
					(this.getChar(next).equals(":")) ||
					(this.getChar(next).equals("+")) ||
					(this.getChar(next).equals("-")) ||
					(this.getChar(next).equals("*")) ||
					(this.getChar(next).equals("/")) ||
					(this.getChar(next).equals(">")) ||
					(this.getChar(next).equals("<")) ||
					(this.getChar(next).equals(" "));
		else
			return true;
	}
	
	public boolean isIdentificador(String s){
		String regEx = "[A-Za-z][A-Za-z0-9_]*";
	    return Pattern.matches(regEx, s);
	}
	
	public boolean isNumber(String s){
		try{
			int i = Integer.parseInt(s);
		}catch(NumberFormatException e){
			return false;
		}
		return true;
	}
	
	//Funcao que adiciona token na tabela, zerando o contador "c" e o lexema
	public void addToken(String tokenType){
		TabelaDeSimbolos.adicionarNaTabela(this.lexema, tokenType, this.linhaPos +1);
		this.c=0;
		this.lexema="";
	}
	
	//Função utilizada para redirecionar um estado na função getTokens
	//Só é utilizado quando a String "lexema" tem o length == 1
	//Caso nada seja aceito, envia para o default, 999, que é quem valida os identificadores
	public void apontador(){
		int x = 0;
		switch(this.lexema){
			case "p":
				x = 1;
				break;
			
			//Tab
			case "	":
				x = 998;
				break;	
			
			//Barra de espaço
			case " ":
				x = 998;
				break;
				
			case "v":
				x=12;
				break;
				
			case "i":
				x=14;
				break;
				
			case "r":
				x=21;
				break;
				
			case "b":
				x=24;
				break;
				
			case "e":
				x=33;
				break;
				
			case "t":
				x=37;
				break;
				
			case "d":
				x=44;
				break;
				
			case "n":
				x=45;
				break;
				
			case "0":
				x=47;				
				break;
				
			case "1":
				x=47;				
				break;
				
			case "2":
				x=47;				
				break;
				
			case "3":
				x=47;				
				break;
				
			case "4":
				x=47;				
				break;
				
			case "5":
				x=47;				
				break;
				
			case "6":
				x=47;				
				break;
				
			case "7":
				x=47;				
				break;
				
			case "8":
				x=47;				
				break;
				
			case "9":
				x=47;				
				break;
				
			case ";":
				x=50;
				break;
				
			case ".":
				x=50;
				break;
				
			case "(":
				x=50;
				break;
				
			case ")":
				x=50;
				break;
				
			case ",":
				x=50;
				break;
				
			case ":":
				x=51;
				break;
				
			case "{":
				x=52;
				break;
				
			case "=":
				x=53;
				break;
				
			case ">":
				x=54;
				break;
				
			case "<":
				x=54;
				break;
				
			case "+":
				x=55;
				break;
				
			case "-":
				x=55;
				break;
				
			case "*":
				x=56;
				break;
				
			case "/":
				x=56;
				break;
				
			case "o":
				x=57;
				break;
				
			case "a":
				x=59;
				break;
					
//				case :
//					break;
									
			default:
				x = 999;
				break;
		}
		this.c =x;
	}
	
	//Função que inicializa as variaveis necessárias, e identificará os tokens
	public void getTokens(){
		//Inicialização
		BufferedReader br = this.leitor.abrirArquivo();
		this.arquivo = this.leitor.lerLinhas(br);
		this.charPos = 0;
		this.linhaPos = 0;
		this.linha = this.arquivo.get(linhaPos);
		this.lexema = "" + this.linha.charAt(charPos);
		
		//Controlador
		int next;
		String nextChar;
		this.c = 0;
		while(this.linhaPos < this.arquivo.size()){
			switch(this.c){
				case 0:
					//Se o lexema for null ou "", ler o próximo
					if(this.lexema.isEmpty()){
						readNext();
					}else{
						//se o lexema estiver preenchido, pegar o resultado da função apontador()
						if(this.lexema.length() >= 1){
							apontador();
						}
					}
					break;
					
				case 1:
					if(!nextIsDelimitador())
						readNext();
					else{
						this.c=999;
						break;
					}
					//As 6 linhas acima são para que as variáveis/identificadores com apenas 1 caractere funcionem.
					//Se repete nos casos em que o controlador(this.c) é apontado a partir da funcao apontador
					
					if(validaLexema("pr")){
						this.c=2;
					}else{
						this.c=999;
					}
					break;
					
				case 2:
					readNext();
					if(validaLexema("pro")){
						this.c=3;
					}else{
						this.c=999;
					}
					break;
					
				case 3:
					readNext();
					if(validaLexema("prog")){
						this.c=4;
					}else{
						if(validaLexema("proc"))
							this.c=7;
						else
							this.c=999;
					}
					break;
					
				case 4:
					readNext();
					if(validaLexema("progr"))
						this.c=5;
					else
						this.c=999;
					break;
					
				case 5:
					readNext();
					if(validaLexema("progra"))
						this.c=6;
					else
						this.c=999;
						
					break;
					
				case 6:
					readNext();
					if(validaLexema("program") && nextIsDelimitador()){
						addToken(Tokens.PR);
					}
					else
						this.c=999;
					break;
					
				case 7:
					readNext();
					if(validaLexema("proce"))
						this.c=8;					
					else
						this.c=999;
					break;
					
				case 8:
					readNext();
					if(validaLexema("proced"))
						this.c=9;
					else
						this.c=999;
					break;
					
				case 9:
					readNext();
					if(validaLexema("procedu"))
						this.c=10;
					else
						this.c=999;
					break;
					
				case 10:
					readNext();
					if(validaLexema("procedur"))
						this.c=11;
					else
						this.c=999;
					break;
					
				case 11:
					readNext();
					if(validaLexema("procedure") && nextIsDelimitador()){
						addToken(Tokens.PR);
					}
					else
						this.c=999;
					break;
					
				case 12:
					if(!nextIsDelimitador())
						readNext();
					else{
						this.c=999;
						break;
					}
					if(validaLexema("va"))
						this.c=13;
					else
						this.c=999;
					break;
					
				case 13:
					readNext();
					if(validaLexema("var") && nextIsDelimitador()){
						addToken(Tokens.PR);
					}
					else
						this.c=999;
					break;
					
				case 14:
					if(!nextIsDelimitador())
						readNext();
					else{
						this.c=999;
						break;
					}
					if(validaLexema("in")){
						this.c=15;
					}else{
						if(validaLexema("if") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}else{
							this.c=999;
						}
					}
					break;
					
					case 15:
						readNext();
						if(validaLexema("int"))
							this.c=16;
						else
							this.c=999;
						break;
						
					case 16:
						readNext();
						if(validaLexema("inte"))
							this.c=17;
						else
							this.c=999;
						break;
						
					case 17:
						readNext();
						if(validaLexema("inte"))
							this.c=18;
						else
							this.c=999;
						break;
						
					case 18:
						readNext();
						if(validaLexema("integ"))
							this.c=19;
						else
							this.c=999;
						break;
					
					case 19:
						readNext();
						if(validaLexema("intege"))
							this.c=20;
						else
							this.c=999;
						break;
						
					case 20:
						readNext();
						if(validaLexema("integer") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}
						else
							this.c=999;
						break;
						
					case 21:
						if(!nextIsDelimitador())
							readNext();
						else{
							this.c=999;
							break;
						}
						if(validaLexema("re"))
							this.c=22;
						else
							this.c=999;
						break;
						
					case 22:
						readNext();
						if(validaLexema("rea"))
							this.c=23;
						else
							this.c=999;
						break;
					
					case 23:
						if(validaLexema("real") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}else
							this.c=999;
						break;
						
					case 24:
						if(!nextIsDelimitador())
							readNext();
						else{
							this.c=999;
							break;
						}
						if(validaLexema("bo")){
							this.c=25;
						}else{
							if(validaLexema("be"))
								this.c=30;
							else
								this.c=999;
						}
						break;
						
					case 25:
						readNext();
						if(validaLexema("boo"))
							this.c=26;
						else
							this.c=999;
						break;
						
					case 26:
						readNext();
						if(validaLexema("bool"))
							this.c=27;
						else
							this.c=999;
						break;
					
					case 27:
						readNext();
						if(validaLexema("boole"))
							this.c=28;
						else
							this.c=999;
						break;
						
					case 28:
						readNext();
						if(validaLexema("boolea"))
							this.c=29;
						else
							this.c=999;
						break;
						
					case 29:
						readNext();
						if(validaLexema("boolean") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}
						else
							this.c=999;
						break;
						
					case 30:
						readNext();
						if(validaLexema("beg"))
							this.c=31;
						else
							this.c=999;
						break;
					
					case 31:
						readNext();
						if(validaLexema("begi"))
							this.c=32;
						else
							this.c=999;
						break;
						
					case 32:
						readNext();
						if(validaLexema("begin") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}else
							this.c=999;
						break;
						
					case 33:
						if(!nextIsDelimitador())
							readNext();
						else{
							this.c=999;
							break;
						}
						if(validaLexema("en")){
							this.c=34;
						}else{
							if(validaLexema("el"))
								this.c=35;
							else
								this.c=999;
						}
						break;
						
					case 34:
						readNext();
						if(validaLexema("end") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}else
							this.c=999;
						
						break;
					
					case 35:
						readNext();
						if(validaLexema("els"))
							this.c=36;
						else
							this.c=999;
						break;
						
					case 36:
						readNext();
						if(validaLexema("else") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}else
							this.c=999;
						break;
						
					case 37:
						if(!nextIsDelimitador())
							readNext();
						else{
							this.c=999;
							break;
						}
						if(validaLexema("th"))
							this.c=38;
						else
							this.c=999;
						break;
						
					case 38:
						readNext();
						if(validaLexema("the"))
							this.c=39;
						else
							this.c=999;
						break;
					
					case 39:
						readNext();
						if(validaLexema("then") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}else
							this.c=999;
						break;
						
					case 40:
						if(!nextIsDelimitador())
							readNext();
						else{
							this.c=999;
							break;
						}
						if(validaLexema("wh"))
							this.c=41;
						else
							this.c=999;
						break;
						
					case 41:
						readNext();
						if(validaLexema("whi"))
							this.c=42;
						else
							this.c=999;
						break;
						
					case 42:
						readNext();
						if(validaLexema("whil"))
							this.c=43;
						else
							this.c=999;
						break;
					
					case 43:
						readNext();
						if(validaLexema("while") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}else
							this.c=999;
						break;
						
					case 44:
						if(!nextIsDelimitador())
							readNext();
						else{
							this.c=999;
							break;
						}
						if(validaLexema("do") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}
						else
							this.c=999;
						break;
						
					case 45:
						if(!nextIsDelimitador())
							readNext();
						else{
							this.c=999;
							break;
						}
						if(validaLexema("no"))
							this.c=46;
						else
							this.c=999;
						break;
						
					case 46:
						if(validaLexema("not") && nextIsDelimitador()){
							addToken(Tokens.PR);
						}else
							this.c=999;
						break;
					
					//Número inteiro
					case 47:
						next = this.charPos+1;
						if(this.linha.length() > next){
							nextChar = getChar(next);
							if(isNumber(nextChar)){
								readNext();
								this.c=47;
								
							}else{
								if(nextChar.equals(".")){
									readNext();
									this.c=48;
								}else{
									if(nextIsDelimitador()){
										addToken(Tokens.NI);
									}else{
										throw new Error("Erro ao identificar número inteiro - " + this.lexema + " - na linha " + this.linhaPos);
									}
								}
							}
						}else{
							if(nextIsDelimitador()){
								addToken(Tokens.NI);
							}
						} 
						break;
					
					//Apontador/validador de numero real	
					case 48:
						next = this.charPos+1;
						if(this.linha.length() > next || !nextIsDelimitador()){
							nextChar = getChar(next);
							if(isNumber(nextChar)){
								this.c=49;
								readNext();
							}
							else
								throw new Error("Erro ao identificar numero real - " + this.lexema + " - na linha " + this.linhaPos);
						}else{
							throw new Error("Erro ao identificar numero real - " + this.lexema + " - na linha " + this.linhaPos);
						}
						break;
						
					//NR	
					case 49:
						next = this.charPos+1;
						if((this.linha.length() > next) && !nextIsDelimitador()){
							nextChar = getChar(next);
							if(isNumber(nextChar)){
								readNext();
								this.c=49;
							}
						}else{
							if(nextIsDelimitador()){
								addToken(Tokens.NR);
							}
						}
						break;
						
					//Aceita todos delimitadores	
					case 50:
						addToken(Tokens.DLM);
						break;
					
					case 51:
						next = this.charPos+1;
						if(!(this.linha.length() > next)){
							addToken(Tokens.DLM);
						}else{
							if(getChar(next).equals("=")){
								readNext();
								addToken(Tokens.ATB);
							}else{
								if(nextIsDelimitador()){
									addToken(Tokens.ATB);
								}
							}
						}
						break;
						
					case 52:
						boolean fechouChaves = false;
						while(!validaLexema("}")){
							if((this.arquivo.size() > this.linhaPos) && (this.linha.length() > this.charPos)){
								this.lexema="";
								readNext();
								if(validaLexema("}"))
									fechouChaves = true;								
							}else{
								break;
							}
							
						}
						if(!fechouChaves)
							throw new Error("Comentário não fechado");
						this.lexema="";
						c=0;
						break;
						
					case 53:
						addToken(Tokens.REL);
						break;
						
					case 54:
						next = this.charPos+1;
						if(!(this.linha.length() > next))
							addToken(Tokens.REL);
						else{
							String nextchareq = "" + getChar(next);
							if(nextchareq.equals("=")){
								readNext();
								addToken(Tokens.REL);
							}else{
								addToken(Tokens.REL);
							}
						}
							
						break;
					
					case 55:
						addToken(Tokens.ADD);
						break;
						
					case 56:
						addToken(Tokens.MUL);
						break;
						
					case 57:
						if(!nextIsDelimitador())
							readNext();
						else{
							this.c=999;
							break;
						}
						
						if(validaLexema("or") && nextIsDelimitador()){
							addToken(Tokens.ADD);
						}else
							this.c=999;
						break;
						
					case 58:
						if(!nextIsDelimitador())
							readNext();
						else{
							this.c=999;
							break;
						}
						
						if(validaLexema("an"))
							this.c=59;
						else
							this.c=999;
						break;
					
					case 59:
						readNext();
						if(validaLexema("and") && nextIsDelimitador()){
							addToken(Tokens.MUL);
						}else
							this.c=999;
						break;
						
//					case :
//						break;
					
				//Barra de espaco	
				case 998:
					this.lexema = "";
					readNext();
					c = 0;
					break;
				
				//Identificador
				case 999:
					while(!nextIsDelimitador() && (this.linha.length() > (this.charPos + 1))){
						readNext();
					}
					if(isIdentificador(this.lexema)){
						addToken(Tokens.ID);						
					}else{
						throw new Error("Simbolo não pertence à linguagem - " + this.lexema + " - na linha " + (this.linhaPos+1));
					}
					break;
			}
		}
	}
}
