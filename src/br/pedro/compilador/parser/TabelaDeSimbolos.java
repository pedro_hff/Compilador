package br.pedro.compilador.parser;

import java.util.ArrayList;

public class TabelaDeSimbolos {
	public static ArrayList<String> tabelaDeSimbolos = new ArrayList<>();
	
	public static void adicionarNaTabela(String lexema, String token, int linha){
		String formater = String.format("%-15s - %-21s - \t%d", lexema, token, linha);
		tabelaDeSimbolos.add(formater);
	}
	
	public static void printTabela(){
		String formater = String.format("%-15s | %-25s | %s", "Lexema", "Token", "Linha");
		System.out.println("___________________________________________________");
		System.out.println(formater);
		System.out.println("___________________________________________________");
		for(String linha : tabelaDeSimbolos)
			System.out.println(linha);
	}
}
