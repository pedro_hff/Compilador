import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Scanner;

import br.pedro.compilador.analisadorlexico.AnalisadorLexico;
import br.pedro.compilador.parser.TabelaDeSimbolos;
import br.pedro.compilador.utils.LeitorDeArquivos;

public class Compilador {

	public static void main(String[] args) {
		System.out.println("Digite o caminho do arquivo a ser lido/compilado: ");
		Scanner scan = new Scanner(System.in);
		String name = scan.next();
		if(!name.equals("x"))
			LeitorDeArquivos.setFileName(name);
		scan.close();
		AnalisadorLexico analisadorLexico = new AnalisadorLexico();
		analisadorLexico.getTokens();
		TabelaDeSimbolos.printTabela();
	}

}
